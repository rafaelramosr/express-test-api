# Express test api 🧐
A simple example of a api using Express JS and JSON data.

## Getting Started 🚀
1. Clone the repository:
```bash
git clone https://gitlab.com/rafaelramosr/express-test-api.git
```

2. Enter the folder:
```bash
cd express-test-api
```

3. Install the dependencies:
```bash
npm install
```

3. Run the local server and you're done:
```bash
npm run start
```
