const { addresses, departments } = require('../data/data.json');
const express = require('express');
const cors = require("cors");
const app = express();
const port = 3000;

app.use(cors());
app.use(express.json());

const delay = ms => new Promise(res => setTimeout(res, ms));
const random = (min, max) => Math.floor((Math.random() * (max - min + 1)) + min);

app.get('/', async (req, res) => {
  await delay(random(1, 3));
  res.send(addresses);
});

app.get('/departments', async (req, res) => {
  await delay(random(1, 3));
  res.send(departments);
});

app.use(function(req, res) {
  const res404 = {
    error: true,
    msg: 'Sorry cant find that!',
    status: 404,
  };
  res.status(404).send(res404);
});

app.listen(port, () => {
  console.log(`Example app listening at http://localhost:${port}`)
})
